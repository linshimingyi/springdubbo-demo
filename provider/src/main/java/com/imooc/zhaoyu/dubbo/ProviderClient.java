package com.imooc.zhaoyu.dubbo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author linshimingyi
 * @package com.imooc.zhaoyu.dubbo
 * @date 2019-02-18 13:43
 */
public class ProviderClient {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext-hello-provider.xml");
        context.start();
        System.in.read(); // 按任意键退出
    }
}
