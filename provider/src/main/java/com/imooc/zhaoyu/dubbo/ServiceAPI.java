package com.imooc.zhaoyu.dubbo;

/**
 * @author linshimingyi
 * @package com.imooc.zhaoyu.dubbo
 * @date 2019-02-18 13:34
 */
public interface ServiceAPI {

    String sendMessage(String message);
}
