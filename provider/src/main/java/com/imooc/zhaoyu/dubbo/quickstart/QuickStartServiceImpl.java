package com.imooc.zhaoyu.dubbo.quickstart;

import com.imooc.zhaoyu.dubbo.ServiceAPI;

/**
 * @author linshimingyi
 * @package com.imooc.zhaoyu.dubbo.quickstart
 * @date 2019-02-18 13:36
 */
public class QuickStartServiceImpl implements ServiceAPI {

    @Override
    public String sendMessage(String message) {
        return "quickstart-provider-message=" + message;
    }
}
